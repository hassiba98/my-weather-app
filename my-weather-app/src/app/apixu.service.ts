import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class ApixuService {
  myApiKey: string = '7b01fde11999a7a527dd213dd0e2699c';

  constructor(private http: HttpClient) { }

  getWeather(location) {
    return this.http.get(
      'https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={API key}'+ this.myApiKey +'&query=' + location
    );
  }
}
